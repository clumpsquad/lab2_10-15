#include <iostream>
#include <limits>

class Trash
{
private:
    void* trash;            ///< ���������� ������

public: 
    /**
     * @brief ��������� ������
     * @param memory ����� ���������� ������
     */
    Trash(size_t memory): trash(new uint8_t[memory])
    {
        std::cout << "Constructor called" << std::endl;
    }

    /**
     * @brief ����������
     */
    ~Trash()
    {
        std::cout << "Destructor called" << std::endl;
        delete[] trash;
    }
};

/**
 * @brief �������, ���������� ����������
 */
void unsafeFunction()
{
    throw std::exception();
}


int main(int argc, char *argv[])
{ 
    try {
        Trash trash = Trash(std::numeric_limits<size_t>::max());
    } catch (std::bad_alloc &e) {
        std::cout << "Bad alloc" << std::endl;
    }

    try {
        static const size_t memorySize = 1600u;
        Trash trash = Trash(memorySize);
        unsafeFunction();
    } catch (std::exception &e) {
        std::cout << "Some exception" << std::endl;
    }
}